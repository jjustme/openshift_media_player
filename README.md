# Media Player
Media Player is a server and web client that is primarily focused on playing radio streams but can also playback locally stored music.

There already exists plenty of software that can playback music and radio streams however I couldn't find any with one specific feature so I decided to write my own. That feature is having a playlist of radio streams with automatically updating song information as the songs playing on the stations changes. Instead of having a playlist which shows you the name of radio stations, it is auto updating to show you the songs playing on those stations.

## Main Features

* Auto updating song information for radio stations
* Control clients from other clients
* Import local music library
* Search music library
* Create and manage playlists
