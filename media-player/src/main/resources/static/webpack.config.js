var path = require("path");
var webpack = require("webpack");

module.exports = {
    entry: './static/js/app.js',
    devtool: 'sourcemaps',
    cache: true,
    debug: true,
    output: {
        path: './static/built',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: "style-loader!css-loader"
            },
            {
                test: /\.(jpg|png)$/,
                loader: "url?limit=25000"
            },
            {
                test: /\.html$/,
                loader: "underscore-template-loader"
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            _: "underscore"
        })
    ]
};