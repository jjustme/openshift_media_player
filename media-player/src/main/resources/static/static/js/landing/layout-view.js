var $ = require("jquery");
var _ = require("underscore");
var Marionette = require("backbone.marionette");
var Radio = require("backbone.radio");

var LobbyView = Marionette.ItemView.extend({
    template: _.template("<%= id %>"),
    tagName: "option",
    attributes: function () {
        return {
            value: this.model.get("id")
        };
    }
});

var LobbiesView = Marionette.CollectionView.extend({
    childView: LobbyView,
    tagName: "select",
    className: "form-control",
    id: "lobbiesSelect"
});

var LayoutView = Marionette.LayoutView.extend({
    template: require("./layout-template.html"),

    regions: {
        list: "div"
    },

    ui: {
        join: ".join",
        create: ".create"
    },

    events: {
        "click @ui.join": "onClickJoin",
        "click @ui.create": "onClickCreate"
    },

    onShow: function () {
        $.ajax({
            url: "api/lobbies"
        }).then(function (lobbies) {
            var models = lobbies.map(function (lobby) {
                return new Backbone.Model(lobby);
            });

            this.getRegion("list").show(new LobbiesView({
                collection: new Backbone.Collection(models)
            }));
        }.bind(this));
    },

    onClickJoin: function () {
        var lobbyId = $("#lobbiesSelect").val();
        console.log("Requesting route change to lobby: " + lobbyId);
        Radio.trigger("router", "route:lobby", lobbyId);
    },

    onClickCreate: function () {
        $.ajax({
            method: "POST",
            url: "api/lobbies"
        }).then(function (createdLobbyId) {
            console.log("Requesting route change to created lobby: " + createdLobbyId);
            Radio.trigger("router", "route:lobby", createdLobbyId);
        });
    }
});

module.exports = LayoutView;