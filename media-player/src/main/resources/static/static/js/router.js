var Backbone = require("backbone");
var Marionette = require("backbone.marionette");
var Radio = require("backbone.radio");
var LandingView = require("./landing/layout-view");
var LobbyView = require("./lobby/layout-view");
var $ = require("jquery");

var regionManager = new Marionette.RegionManager({
    regions: {
        main: "#app-body"
    }
});

var Controller = Marionette.Controller.extend({
    routerChannel: Radio.channel("router"),

    initialize: function() {
        this.routerChannel.on("route:lobby", this.routeLobby);
    },

    routeLobby: function(lobbyId) {
        console.log("Received request for route change to lobby: " + lobbyId);
        Backbone.history.navigate("lobby/" + lobbyId, {trigger: true});
    },

    showLobbies: function () {
        var layout = new LandingView();
        regionManager.get("main").show(layout);
    },

    showLobby: function(lobbyId) {
        var layout = new LobbyView({lobbyId: lobbyId});
        regionManager.get("main").show(layout);
    }
});

var Router = Marionette.AppRouter.extend({
    controller: new Controller(),
    appRoutes: {
        "": "showLobbies",
        "lobby/:id": "showLobby"
    }
});

module.exports = Router;