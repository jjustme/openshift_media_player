var Backbone = require("backbone");
var Marionette = require("backbone.marionette");

var Router = require("./router");

var app = new Marionette.Application({
    onStart: function() {
        var router = new Router();

        Backbone.history.start();
    }
});

app.start();