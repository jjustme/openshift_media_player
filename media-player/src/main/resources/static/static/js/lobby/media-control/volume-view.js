var $ = require("jquery");
var _ = require("underscore");
var Marionette = require("backbone.marionette");

var VolumeView = Marionette.ItemView.extend({
    template: require("./volume-template.html"),
    attributes: {
        style: "display: flex; justify-content: center; align-items: center; height: 100%"
    },

    ui: {
        slider: "input"
    },

    events: {
        "input @ui.slider": "changeVolume"
    },

    initialize: function() {
        this.options.volumeHander = _.throttle(function (volume, lobbyId) {
            $.ajax({
                method: "PUT",
                url: "/api/lobbies/" + lobbyId + "/volume",
                processData: false,
                contentType: "application/json",
                data: volume.toString()
            });
        }, 150);
    },

    onShow: function() {
        this.listenTo(this.model, "change", this.onChange);
    },

    onChange: function() {
        this.render();
    },

    changeVolume: function(evt) {
        var volume = this.ui.slider.val();
        var lobbyId = this.options.lobbyId;
        console.debug("Changing volume to: " + volume + " in lobby: " + lobbyId);
        this.options.volumeHander(volume, lobbyId);
    }
});

module.exports = VolumeView