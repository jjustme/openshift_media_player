var $ = require("jquery");
var Backbone = require("backbone");
var Marionette = require("backbone.marionette");
var ManageTitleListView = require("./manage-title-list-view");

var ManageLibraryView = Marionette.LayoutView.extend({
    template: require("./manage-library-layout.html"),

    ui: {
        manageLibraryButton: "#manage-library-button",
        modal: ".modal",
        close: ".close",
        closeButton: ".close-button",
        search: "form",
        query: "#query"
    },

    regions: {
        titleList: "#manage-title-list"
    },

    events: {
        "click @ui.manageLibraryButton": "showAddTitleModal",
        "click @ui.close": "closeModal",
        "click @ui.closeButton": "closeModal",
        "click @ui.modal": "closeModal",
        "submit @ui.search": "searchTitles"
    },

    showAddTitleModal: function () {
        this.ui.modal.css("display", "block");

        $.ajax({
            url: "/api/titles"
        }).then(function (titles) {
            this.updateTitles(titles);
        }.bind(this));
    },

    updateTitles: function (titles) {
        var region = this.getRegion("titleList");

        var models = titles.map(function (title) {
            Object.assign(title, {
                titleId: title.id
            });
            delete title.id;
            return new Backbone.Model(title);
        }.bind(this));

        if (region.currentView) {
            region.currentView.collection.reset(models);
        } else {
            region.show(new ManageTitleListView({
                collection: new Backbone.Collection(models)
            }));
        }
    },

    closeModal: function (evt) {
        // only direct element clicks,
        // not bubbled up events should close the modal
        if (evt.target == evt.currentTarget) {
            this.ui.modal.css("display", "none");
        }
    },

    searchTitles: function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var query = this.ui.query.val();
        console.debug("Filtering titles with query: " + query);
        $.ajax({
            url: "/api/titles",
            data: {
                q: query
            }
        }).then(function(titles) {
            this.updateTitles(titles);
        }.bind(this));
    }
});

module.exports = ManageLibraryView;