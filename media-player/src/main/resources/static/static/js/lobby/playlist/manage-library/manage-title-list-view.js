var $ = require("jquery");
var Marionette = require("backbone.marionette");
var TitleView = require("../title-view");
var Radio = require("backbone.radio");

var AddTitleListView = Marionette.CollectionView.extend({
    childView: TitleView,
    tagName: "div",
    className: "list-group",

    events: {
        "click a": "addTitle"
    },

    initialize: function() {
        this.options.channel = Radio.channel("ws");
    },

    addTitle: function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var playlistId = this.options.channel.request("requestPlaylistId");
        var titleId = $(evt.currentTarget).attr("key");
        console.debug("Adding title: " + titleId + " to playlist: " + playlistId);
        $.ajax({
            method: "PUT",
            url: "/api/playlists/" + playlistId + "/add-title",
            processData: false,
            contentType: "application/json",
            data: titleId.toString()
        });
    },

    onChildviewRemoveTitle: function(args) {
        var titleId = args.model.get("titleId");
        console.debug("Removing title: " + titleId + " from library.");
        $.ajax({
            method: "DELETE",
            url: "/api/titles/" + titleId
        }).then(function() {
            this.collection.remove(args.model);
        }.bind(this));
    }
});

module.exports = AddTitleListView;