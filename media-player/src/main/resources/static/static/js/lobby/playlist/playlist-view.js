var $ = require("jquery");
var Marionette = require("backbone.marionette");
var TitleView = require("./title-view");
var Radio = require("backbone.radio");
var ManageLibraryView = require("./manage-library/manage-library-view");
var TitleListView = require("./title-list-view");

var PlaylistView = Marionette.LayoutView.extend({
    template: require("./playlist-template.html"),

    regions: {
        manageLibrary: "#manage-library",
        titleList: "#titleList"
    },

    onShow: function () {
        var channel = Radio.channel("ws");
        var handlers = {
            "update:playlist": this.showPlaylist.bind(this),
            "update:title": this.updateTitle.bind(this),
            "update:nowPlayingInfo": this.updateNowPlayingTitle.bind(this)
        };

        channel.on(handlers);

        this.options.channel = channel;
        this.options.handlers = handlers;

        this.getRegion("manageLibrary").show(new ManageLibraryView());
    },

    onBeforeDestory: function () {
        this.options.channel.off(this.options.handlers);
    },

    showPlaylist: function (playlist) {
        var region = this.getRegion("titleList");

        var nowPlayingInfo = this.options.channel.request("requestNowPlayingInfo");
        var isNowPlayingPlaylist = false;
        var nowPlayingPlaylistTitleIndex = null;
        if (nowPlayingInfo) {
            isNowPlayingPlaylist = nowPlayingInfo.playlistId == playlist.id;
            nowPlayingPlaylistTitleIndex = nowPlayingInfo.titleIndex;
        }
        var models = playlist.titles.map(function (title, index) {
            Object.assign(title, {
                titleId: title.id,
                index: index,
                playing: (index == nowPlayingPlaylistTitleIndex) && isNowPlayingPlaylist
            });
            delete title.id;
            return new Backbone.Model(title);
        });

        if (!region.currentView || !region.currentView.collection) {
            var playlistView = new TitleListView({
                collection: new Backbone.Collection(models),
                id: playlist.id,
                lobbyId: this.options.lobbyId
            });

            region.show(playlistView);
        } else {
            region.currentView.collection.reset(models);
            region.currentView.options.id = playlist.id;
        }
    },

    updateTitle: function (title) {
        var region = this.getRegion("titleList");

        var container = region.currentView.children;
        container.forEach(function (titleView) {
            var current = titleView.model.get("titleId") == title.id;
            if (current) {
                var updated = Object.assign({}, title, {
                    titleId: title.id,
                    index: titleView.model.get("index"),
                    playing: titleView.model.get("playing")
                });
                delete updated.id;
                titleView.model.set(updated);
            }
        });
    },

    updateNowPlayingTitle: function (nowPlayingInfo) {
        var region = this.getRegion("titleList");

        if (region.currentView == null) return;

        var isNowPlayingPlaylist = this.options.channel.request("requestPlaylistId") == nowPlayingInfo.playlistId;
        var container = region.currentView.children;
        container.forEach(function (titleView, index) {
            var current = index == nowPlayingInfo.titleIndex && isNowPlayingPlaylist;

            if (titleView.model.get("playing") && !current) {
                titleView.model.set({ playing: false });
            }

            if (current) {
                titleView.model.set({ playing: true });
            }
        });
    }
});

module.exports = PlaylistView;