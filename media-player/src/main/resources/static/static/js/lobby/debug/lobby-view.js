var Marionette = require("backbone.marionette");

var LobbyView = Marionette.ItemView.extend({
    template: require("./lobby-view.html"),

    initialize: function () {
        this.listenTo(this.model, "change", this.onChange);
    },

    onChange: function () {
        this.render();
    }
});

module.exports = LobbyView;