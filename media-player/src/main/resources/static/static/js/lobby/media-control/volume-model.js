var Backbone = require("backbone");

var VolumeModel = Backbone.Model.extend({
    defaults: {
        volume: 50
    }
});

module.exports = VolumeModel;