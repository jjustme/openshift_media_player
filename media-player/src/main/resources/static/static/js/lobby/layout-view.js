var $ = require("jquery");
var _ = require("underscore");
var Marionette = require("backbone.marionette");
var Radio = require("backbone.radio");
var MPClient = require("./client");
var MediaControlView = require("./media-control/media-control-view");
var PlaylistTabsView = require("./playlist-tabs/playlist-tabs-view");
var PlaylistView = require("./playlist/playlist-view");
var LobbyView = require("./debug/lobby-view");

var Layout = Marionette.LayoutView.extend({
    template: require("./layout-template.html"),

    regions: {
        nowPlaying: "#now-playing",
        playlistTabs: "#playlist-tabs",
        playlist: "#playlist-wrapper",
        debug: "#debug"
    },

    onShow: function () {
        this.options.client = new MPClient("/ws", this.options.lobbyId);
        this.options.client.connect();

        this.options.channel = Radio.channel("ws");
        var channel = this.options.channel;
        channel.on("update:lobby", function (lobby) {
            this.showLobby(lobby);
        }.bind(this));

        this.getRegion("nowPlaying").show(new MediaControlView({
            lobbyId: this.options.lobbyId
        }));

        this.getRegion("playlistTabs").show(new PlaylistTabsView({
            lobbyId: this.options.lobbyId
        }));

        this.getRegion("playlist").show(new PlaylistView({
            lobbyId: this.options.lobbyId
        }));
    },

    onBeforeDestroy: function () {
        this.options.client.disconnect();
        this.options.channel.reset();
    },

    showLobby: function (lobby) {
        var region = this.getRegion("debug");
        if (!region.currentView || !region.currentView.model) {
            var lobbyView = new LobbyView({
                model: new Backbone.Model(lobby)
            });

            region.show(lobbyView);
        } else {
            var model = region.currentView.model;
            model.set(lobby);
        }
    }
});

module.exports = Layout;