var $ = require("jquery");
var Backbone = require("backbone");
var Marionette = require("backbone.marionette");
var Radio = require("backbone.radio");
var NowPlayingView = require("./now-playing-view");
var TitleModel = require("./title-model");
var VolumeView = require("./volume-view");
var VolumeModel = require("./volume-model");
var Player = require("./player");

var MediaControlView = Marionette.LayoutView.extend({
    template: require("./media-control-layout.html"),
    attributes: {
        style: "height: 50px"
    },

    events: {
        "click #stop-playback-button": "stopPlayback"
    },

    regions: {
        nowPlaying: ".nowPlaying-view",
        volume: ".volume-control"
    },

    onShow: function () {
        var channel = Radio.channel("ws");
        var handlers = {
            "update:lobby": this.onLobbyUpdate.bind(this),
            "update:nowPlayingTitle": this.onNowPlayingTitleUpdate.bind(this)
        };

        channel.on(handlers);
        this.options.channel = channel;
        this.options.handlers = handlers;

        this.getRegion("volume").show(new VolumeView({
            model: new VolumeModel(),
            lobbyId: this.options.lobbyId
        }));

        this.options.player = new Player();
        this.options.player.start();
    },

    onBeforeDestroy: function () {
        this.options.channel.off(this.options.handlers);
        this.options.player.stop();
    },

    onLobbyUpdate: function (lobby) {
        console.debug("Lobby updated, volume: " + lobby.volume);
        this.options.player.setVolume(lobby.volume);
        this.getRegion("volume").currentView.model.set({
            volume: lobby.volume
        });
    },

    onNowPlayingTitleUpdate: function (title) {
        var region = this.getRegion("nowPlaying");

        region.show(new NowPlayingView({
            model: new TitleModel(title)
        }));
    },

    stopPlayback: function () {
        var lobbyId = this.options.lobbyId;
        console.debug("Stopping playback in lobby: " + lobbyId);
        $.ajax({
            method: "PUT",
            url: "/api/lobbies/" + lobbyId + "/now-playing",
            processData: false,
            contentType: "application/json",
            data: null
        });
    }
});

module.exports = MediaControlView;