var Radio = require("backbone.radio");

var Player = function () {
    this.channel = null;

    var createAudioElement = function (volume) {
        var audio = new Audio();
        audio.autoplay = true;
        audio.volume = volume != undefined ? volume / 100 : 0.5;
        audio.onended = function() {
            // play next
            if (this.channel) {
                console.debug("Player trigger playback ended event, id: " + this.titleId);
                this.channel.trigger("playbackEnded", this.titleId);
            }
        }.bind(this);
        return audio;
    }.bind(this);

    this.audio = createAudioElement();
    this.titleId = null;

    var stopPlayback = function () {
        this.audio.pause();
        this.audio.src = "";
        this.audio.currentTime = 0;
        this.audio = createAudioElement(this.audio.volume * 100);
    }.bind(this);

    var onPlayingStateUpdated = function (title) {
        if (!title) {
            this.titleId = null;
            stopPlayback();
            return;
        }
        this.titleId = title.id;

        var expectedSrc = "/api/play/" + title.id;
        if (!this.audio.src.endsWith(expectedSrc)) {
            stopPlayback();
            this.audio.src = expectedSrc;
        } else if (this.audio.paused) {
            this.audio.play();
        }
    }.bind(this);

    var setVolume = function(volume) {
        this.audio.volume = volume / 100;
    }.bind(this);

    var startPlayer = function () {
        this.channel = Radio.channel("ws");
        this.channel.on("update:nowPlayingTitle", onPlayingStateUpdated);
    }.bind(this);

    var stopPlayer = function () {
        this.channel.off("update:nowPlayingTitle", onPlayingStateUpdated);
    }.bind(this);

    return {
        start: startPlayer,
        stop: stopPlayer,
        setVolume: setVolume
    }
};

module.exports = Player;
