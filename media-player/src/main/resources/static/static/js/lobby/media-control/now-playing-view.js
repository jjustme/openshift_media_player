var Marionette = require("backbone.marionette");

var NowPlayingView = Marionette.ItemView.extend({
    template: require("./now-playing-layout.html"),
    attributes: {
        style: "display: flex; justify-content: center; align-items: center; height: 100%; border-style: outset"
    },

    onShow: function() {
        this.listenTo(this.model, "change", this.onChange);
    },

    onChange: function() {
        this.render();
    }
});

module.exports = NowPlayingView;