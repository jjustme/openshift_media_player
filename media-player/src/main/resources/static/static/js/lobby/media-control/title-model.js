var Backbone = require("backbone");

var TitleModel = Backbone.Model.extend({
    defaults: {
        id: null,
        isStream: true,
        provider: "",
        title: "",
        uri: ""
    }
});

module.exports = TitleModel;