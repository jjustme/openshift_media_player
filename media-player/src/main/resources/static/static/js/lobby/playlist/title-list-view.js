var $ = require("jquery");
var Marionette = require("backbone.marionette");
var Radio = require("backbone.radio");
var TitleView = require("./title-view");

var TitleListView = Marionette.CollectionView.extend({
    childView: TitleView,
    tagName: "div",
    className: "list-group",

    events: {
        "click a": "onClick"
    },

    initialize: function () {
        this.options.channel = Radio.channel("ws");
    },

    onClick: function (evt) {
        evt.preventDefault();
        var index = $(evt.currentTarget).attr("index");
        var lobbyId = this.options.lobbyId;
        var playlistId = this.options.channel.request("requestPlaylistId");
        if (!playlistId) {
            console.log("Could not get current playlist id to being playback");
            return;
        }

        console.debug("Starting playback of title at index: " + index + " in lobby: " + lobbyId);
        $.ajax({
            method: "PUT",
            url: "/api/lobbies/" + lobbyId + "/now-playing",
            contentType: "application/json",
            processData: false,
            data: JSON.stringify({ playlistId: playlistId, titleIndex: index })
        });
    },

    onChildviewRemoveTitle: function (child, index) {
        console.debug("Removing title at index: " + index + " of playlist: " + this.options.id);
        $.ajax({
            method: "PUT",
            url: "/api/playlists/" + this.options.id + "/remove-title",
            processData: false,
            contentType: "application/json",
            data: index.toString()
        });
    }
});

module.exports = TitleListView;