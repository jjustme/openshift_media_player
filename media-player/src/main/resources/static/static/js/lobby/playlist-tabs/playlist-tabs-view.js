var $ = require("jquery");
var Backbone = require("backbone");
var Marionette = require("backbone.marionette");
var Radio = require("backbone.radio");

var AddPlaylistView = Marionette.ItemView.extend({
    tagName: "li",
    template: require("./playlist-add-tab-template.html"),
    attributes: {
        role: "presentation"
    },

    events: {
        "click a": "onClick"
    },

    onClick: function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        $.ajax({
            method: "POST",
            url: "/api/playlists",
            contentType: "application/json"
        }).then(function(){
            this.triggerMethod("refresh");
        }.bind(this));
    }
});

var PlaylistTabView = Marionette.ItemView.extend({
    tagName: "li",
    template: require("./playlist-tab-template.html"),
    attributes: {
        role: "presentation"
    },

    events: {
        "click a": "onClick"
    },

    onShow: function () {
        var channel = Radio.channel("ws");
        var handlers = {
            "update:lobby": this.onLobbyUpdate.bind(this)
        };

        channel.on(handlers);

        this.options.channel = channel;
        this.options.handlers = handlers;

        var selectedPlaylistId = this.options.channel.request("requestPlaylistId");
        this.onLobbyUpdate({ playlistId: selectedPlaylistId });
    },

    onBeforeDestory: function () {
        this.options.channel.off(this.options.handlers);
    },

    onClick: function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        $.ajax({
            method: "PUT",
            url: "/api/lobbies/" + this.model.get("lobbyId") + "/playlist",
            processData: false,
            contentType: "application/json",
            data: this.model.get("id").toString()
        });
    },

    onLobbyUpdate: function (lobby) {
        if (lobby.playlistId == this.model.get("id")) {
            this.$el.addClass("active");
        } else {
            this.$el.removeClass("active");
        }
    }
});

var PlaylistTabsView = Marionette.CollectionView.extend({
    tagName: "ol",
    getChildView: function (item) {
        if (item.get("last")) {
            return AddPlaylistView
        }
        return PlaylistTabView;
    },
    attributes: {
        class: "nav nav-tabs"
    },

    initialize: function () {
        this.collection = new Backbone.Collection();
    },

    onShow: function () {
        $.ajax({
            url: "/api/playlists"
        }).then(function (playlists) {
            playlists.push({ last: true }); // placeholder for the add playlist button
            var models = playlists.map(function (playlist) {
                Object.assign(playlist, {
                    lobbyId: this.options.lobbyId
                });

                return new Backbone.Model(playlist);
            }.bind(this));

            this.collection.reset(models);
        }.bind(this));
    },

    onChildviewRefresh: function() {
        this.onShow();
    }
});

module.exports = PlaylistTabsView;