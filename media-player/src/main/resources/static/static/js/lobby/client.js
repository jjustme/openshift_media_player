var $ = require("jquery");
var SockJS = require("sockjs-client");
var Radio = require("backbone.radio");

function MPClient(endpoint, lobbyId) {
    this.endpoint = endpoint;
    this.socket = null;
    this.stompClient = null;
    this.channel = Radio.channel("ws");

    this.state = {
        lobbyId: lobbyId,
        lobby: {},
        playlistId: null,
        playlist: [],
        nowPlayingTitleId: null,
        nowPlayingTitle: {}
    };

    this.channel.reply("requestPlaylistId", function () {
        return this.state.playlistId;
    }.bind(this));

    this.channel.reply("requestNowPlayingTitleId", function () {
        return this.state.nowPlayingTitleId;
    }.bind(this));

    this.channel.reply("requestNowPlayingInfo", function () {
        if (!this.state.lobby) {
            return null;
        } else {
            var lobby = this.state.lobby;
            return {
                playlistId: lobby.nowPlayingPlaylistId,
                titleIndex: lobby.nowPlayingTitleIndex
            }
        }
    }.bind(this));

    this.channel.on("playbackEnded", function (titleId) {
        if (!this.state.lobby) {
            console.log("Cannot play next title, no lobby loaded.");
            return;
        }

        if (titleId != this.state.nowPlayingTitleId) {
            console.log("Unexpected title id, not playing next title in playlist.");
            return;
        }

        var currentIndex = this.state.lobby.nowPlayingTitleIndex;
        var data = {
            playlistId: this.state.lobby.nowPlayingPlaylistId,
            titleIndex: currentIndex + 1
        };

        var callback = function (playlist) {
            if (currentIndex >= playlist.length - 1) {
                console.log("End of the playlist has been reached stopping playback");
                data = null;
            }

            console.debug("Changing now playing title in lobby: " + this.state.lobbyId);
            $.ajax({
                method: "PUT",
                url: "/api/lobbies/" + this.state.lobbyId + "/now-playing",
                contentType: "application/json",
                processData: false,
                data: JSON.stringify(data)
            });
        }.bind(this);

        if (this.state.lobby.nowPlayingPlaylistId != this.state.playlistId) {
            $.ajax({
                url: "/api/playlists/" + this.state.lobby.nowPlayingPlaylistId
            }).then(function (playlist) {
                callback(playlist);
            }.bind(this));
        } else {
            callback(this.state.playlist);
        }
    }.bind(this));

    var getState = function () {
        return Object.assign({}, this.state);
    }.bind(this);

    var notifyNowPlayingInfo = function (lobby) {
        this.channel.trigger("update:nowPlayingInfo", {
            playlistId: lobby.nowPlayingPlaylistId,
            titleIndex: lobby.nowPlayingTitleIndex
        });
    }.bind(this);

    var loadState = function (lobbyId) {
        return $.ajax({ url: "/api/lobbies/" + lobbyId }).then(function (lobby) {
            var shouldLoadPlaylist = false;
            var shouldLoadNowPlayingTitle = false;
            if (this.state.playlistId != lobby.playlistId) {
                shouldLoadPlaylist = true;
            }

            if (this.state.lobby) {
                // has the nowPlayingPlaylistId or nowPlayingTitleIndex changed
                if ((this.state.lobby.nowPlayingPlaylistId != lobby.nowPlayingPlaylistId) || (this.state.lobby.nowPlayingTitleIndex != lobby.nowPlayingTitleIndex)) {
                    notifyNowPlayingInfo(lobby);
                }
            } else {
                notifyNowPlayingInfo(lobby);
            }

            if (this.state.nowPlayingTitleId != lobby.nowPlayingTitleId) {
                shouldLoadNowPlayingTitle = true;
            }

            Object.assign(this.state, {
                lobby: lobby,
                playlistId: lobby.playlistId,
                nowPlayingTitleId: lobby.nowPlayingTitleId
            });
            this.channel.trigger("update:lobby", lobby);

            var initialNowPlayingTitleLoad = function () {
                if (shouldLoadNowPlayingTitle && !lobby.nowPlayingTitleId) {
                    this.state.nowPlayingTitle = {};
                    this.channel.trigger("update:nowPlayingTitle", null);
                } else if (shouldLoadNowPlayingTitle) {
                    var existsInPlaylist = false;

                    this.state.playlist.some(function (title) {
                        if (title.id == this.state.nowPlayingTitleId) {
                            existsInPlaylist = true;
                            this.state.nowPlayingTitle = title;
                        }
                        return existsInPlaylist;
                    }.bind(this));

                    if (existsInPlaylist) {
                        this.channel.trigger("update:nowPlayingTitle", this.state.nowPlayingTitle);
                    } else {
                        loadTitle(this.state.nowPlayingTitleId);
                    }
                }
            }.bind(this);

            if (shouldLoadPlaylist) {
                loadPlaylist(this.state.playlistId).then(initialNowPlayingTitleLoad);
            } else {
                initialNowPlayingTitleLoad();
            }
        }.bind(this));
    }.bind(this);

    var loadPlaylist = function (playlistId) {
        return $.ajax({ url: "/api/playlists/" + playlistId }).then(function (playlist) {
            Object.assign(this.state, {
                playlistId: playlist.id,
                playlist: playlist.titles
            });
            this.channel.trigger("update:playlist", playlist);
        }.bind(this));
    }.bind(this);

    var loadTitle = function (titleId) {
        return $.ajax({ url: "/api/titles/" + titleId }).then(function (updatedTitle) {
            this.state.playlist.forEach(function (title, index) {
                if (title.id == updatedTitle.id) {
                    this.state.playlist[index] = updatedTitle;
                }
            }.bind(this));

            this.channel.trigger("update:title", updatedTitle);

            if (this.state.nowPlayingTitleId == updatedTitle.id) {
                this.state.nowPlayingTitle = updatedTitle;
                this.channel.trigger("update:nowPlayingTitle", updatedTitle);
            }
        }.bind(this));
    }.bind(this);

    var initialise = function () {
        loadState(this.state.lobbyId).then(function () {
            // listen to websocket
            this.stompClient.subscribe("/topic/lobby/" + this.state.lobbyId, function (msg) {
                var notification = JSON.parse(msg.body);
                loadState(notification.id);
            }.bind(this));

            this.stompClient.subscribe("/topic/playlist", function (msg) {
                var notification = JSON.parse(msg.body);
                if (notification.id == this.state.playlistId) {
                    loadPlaylist(notification.id);
                }
            }.bind(this));

            this.stompClient.subscribe("/topic/title", function (msg) {
                var notification = JSON.parse(msg.body);
                loadTitle(notification.id);
            }.bind(this));
        }.bind(this));
    }.bind(this);

    var connect = function () {
        var deferred = $.Deferred();
        this.socket = new SockJS(this.endpoint);
        this.stompClient = Stomp.over(this.socket);

        this.stompClient.connect({}, function () {
            deferred.resolve();
            this.channel.trigger("status", "connected");

            initialise();
        }.bind(this), function (err) {
            deferred.reject();
            this.channel.trigger("status", "reconnecting");
            setTimeout(function () {
                connect().then(function () {
                    location.reload();
                });
            }, 5000)
        }.bind(this));

        return deferred.promise();
    }.bind(this);

    var disconnect = function () {
        this.stompClient.disconnect();
        this.channel.stopReplying("requestPlaylistId");
        this.channel.stopReplying("requestNowPlayingTitleId");
        this.channel.stopReplying("requestNowPlayingInfo");
    }.bind(this);

    return {
        connect: connect,
        disconnect: disconnect,
        getState: getState
    }
};

module.exports = MPClient;
