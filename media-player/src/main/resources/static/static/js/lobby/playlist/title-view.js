var Marionette = require("backbone.marionette");

var TitleView = Marionette.ItemView.extend({
    template: require("./title-template.html"),
    tagName: "a",
    className: "list-group-item",
    attributes: function () {
        return {
            href: "#",
            index: this.model.get("index"),
            key: this.model.get("titleId")
        }
    },

    events: {
        "click button": "onClickRemove"
    },

    initialize: function () {
        this.listenTo(this.model, "change", this.onChange);
    },

    onRender: function () {
        if (this.model.get("playing")) {
            this.$el.addClass("active");
        } else {
            this.$el.removeClass("active");
        }

        console.debug("title id: " + this.model.get("titleId") + " index: " + this.model.get("index"));
    },

    onClickRemove: function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        this.triggerMethod("remove:title", this.model.get("index"));
    },

    onChange: function () {
        this.render();
    }
});

module.exports = TitleView;