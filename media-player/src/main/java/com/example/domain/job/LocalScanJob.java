package com.example.domain.job;

import com.example.AppConfiguration;
import com.example.domain.title.Provider;
import com.example.domain.title.Title;
import com.example.domain.title.TitleService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;


/**
 * Created on 02/06/16.
 */
public class LocalScanJob implements Job {
    private static Logger log = LoggerFactory.getLogger(LocalScanJob.class);

    private static JobType type = JobType.LOCAL_SCAN;

    private TitleService titleService;

    private String[] paths = null;

    @Autowired
    public LocalScanJob(TitleService titleService, AppConfiguration configuration) {
        this.titleService = titleService;

        this.paths = configuration.getPaths().split(",");
        if (ObjectUtils.isEmpty(this.paths)) {
            throw new IllegalArgumentException("Paths parameter is null or empty, scan cannot be performed");
        }
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("Local scan job executing...");

        for (String pathString : this.paths) {
            log.info("Scanning path '{}'", pathString);

            try {
                Files.walkFileTree(Paths.get(pathString), new CustomFileVisitor(this.titleService));
            } catch (IOException e) {
                log.warn("Scanning path '{}' threw exception: '{}'", pathString, e.getMessage());
            }
        }
    }

    public static JobType getType() {
        return type;
    }

    private class CustomFileVisitor extends SimpleFileVisitor<Path> {
        private final Logger log = LoggerFactory.getLogger(CustomFileVisitor.class);

        private final String[] SUPPORTED_EXTENSIONS = {
                ".mp3",
                ".flac"
        };

        private TitleService titleService;

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            log.debug("Processing file '{}'", file.toAbsolutePath());

            if (attrs.isRegularFile()) {
                for (String extension : SUPPORTED_EXTENSIONS) {
                    if (file.toString().endsWith(extension)) {
                        // supported file, notify listeners the real path
                        Title title = new Title();
                        title.setProvider(Provider.LOCAL);
                        title.setIsStream(false);
                        title.setUri(file.toRealPath().toString());

                        String filename = file.getFileName().toString();
                        filename = filename.substring(0, filename.length() - extension.length());
                        title.setTitle(filename);

                        try {
                            this.titleService.addIfNotExists(title);
                        } catch (DataIntegrityViolationException exception) {
                            log.warn("Exception '{}' occured adding file: '{}'", exception.getMessage(), title.getUri());
                        }

                        break;
                    }
                }
            }

            return FileVisitResult.CONTINUE;
        }

        public CustomFileVisitor(TitleService titleService) {
            this.titleService = titleService;
        }
    }
}
