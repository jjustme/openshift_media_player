package com.example.domain.job;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created on 01/06/16.
 */
public interface JobResultRepository extends JpaRepository<JobResult, Long> {
}
