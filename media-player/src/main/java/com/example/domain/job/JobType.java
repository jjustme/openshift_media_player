package com.example.domain.job;

/**
 * Created on 01/06/16.
 */
public enum JobType {
    LOCAL_SCAN,
    FETCH_TUNEIN
}
