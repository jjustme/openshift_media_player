package com.example.domain.job;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * Created on 02/06/16.
 */

@Component
public class StdSchedulerFactoryWrapper {
    private SchedulerFactory schedulerFactory;

    @Autowired
    public StdSchedulerFactoryWrapper(JobFactory jobFactory) throws SchedulerException {
        this.schedulerFactory = new StdSchedulerFactory();
        this.schedulerFactory.getScheduler().setJobFactory(jobFactory);
        this.schedulerFactory.getScheduler().start();
    }

    @Bean
    public Scheduler getScheduler() throws SchedulerException {
        return this.schedulerFactory.getScheduler();
    }

    public SchedulerFactory getSchedulerFactory() {
        return schedulerFactory;
    }
}
