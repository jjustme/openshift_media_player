package com.example.domain.job;

import com.example.domain.title.Provider;
import com.example.domain.title.Title;
import com.example.domain.title.TitleNotification;
import com.example.domain.title.TitleService;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.fn.Consumer;

import java.util.Collections;
import java.util.List;

import static reactor.bus.selector.Selectors.$;

/**
 * Created on 04/06/16.
 */

@Service
public class FetchService implements Consumer<Event<TitleNotification>>, CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(FetchService.class);

    private Scheduler scheduler;

    private EventBus eventBus;

    private TitleService titleService;

    @Bean
    public static EventBus createEventBus() {
        return EventBus.create();
    }

    @Autowired
    public FetchService(Scheduler scheduler, EventBus eventBus, TitleService titleService) {
        this.scheduler = scheduler;
        this.eventBus = eventBus;
        this.titleService = titleService;
    }

    @Override
    public void accept(Event<TitleNotification> titleNotificationEvent) {
        TitleNotification notification = titleNotificationEvent.getData();

        switch (notification.getAction()) {

            case CREATE:
                this.startFetching(notification.getId());
                break;
            case DELETE:
                this.stopFetching(notification.getId());
                break;
            default:
                // do nothing
        }
    }

    private void startFetching(Long id) {
        Title title = this.titleService.findOne(id);

        if (title == null) {
            log.warn("Title with id '{}' not found, cannot start fetching.", id);
            return;
        }

        Class<? extends Job> jobClass = null;

        switch (title.getProvider()) {

            case TUNEIN:
                jobClass = TuneInFetchJob.class;
                break;
            default:
                // do nothing
        }

        if (jobClass == null) {
            log.info("No fetcher found for title with id '{}' and uri '{}'.", title.getId(), title.getUri());
            return;
        }

        JobDetail fetchJob = JobBuilder.newJob(jobClass)
                .withIdentity(id.toString())
                .usingJobData("titleId", id)
                .build();

        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(id.toString())
                .startNow()
                .build();

        try {
            this.scheduler.scheduleJob(fetchJob, trigger);
        } catch (SchedulerException e) {
            log.warn("Error encountered which scheduling fetching job: '{}'", e.getMessage());
        }
    }

    private void stopFetching(Long id) {
        try {
            this.scheduler.deleteJob(JobKey.jobKey(id.toString()));
        } catch (SchedulerException e) {
            log.warn("Exception occured when unscheduling job: '{}'", e.getMessage());
        }
    }

    @Override
    public void run(String... strings) throws Exception {
        this.eventBus.on($("title"), this);

        this.startFetchingForAllTitles();
    }

    private void startFetchingForAllTitles() {
        List<Provider> providers = Collections.singletonList(Provider.TUNEIN);

        for (Provider provider : providers) {
            List<Title> titles = this.titleService.findByProvider(provider);

            for (Title title : titles) {
                this.startFetching(title.getId());
            }
        }
    }
}
