package com.example.domain.job;

/**
 * Created on 01/06/16.
 */

public class CreateJob {
    private JobType type;

    public JobType getType() {
        return type;
    }

    public void setType(JobType type) {
        this.type = type;
    }
}
