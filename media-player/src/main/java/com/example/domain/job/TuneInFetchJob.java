package com.example.domain.job;

import com.example.domain.title.Title;
import com.example.domain.title.TitleService;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

/**
 * Created on 04/06/16.
 */
public class TuneInFetchJob implements Job {

    private static final Logger log = LoggerFactory.getLogger(TuneInFetchJob.class);

    private static final String NOW_PLAYING_URL_FORMAT = "https://feed.tunein.com/profiles/s%s/nowplaying?partnerId=RadioTime&_=%f";

    private TitleService titleService;

    @Autowired
    public TuneInFetchJob(TitleService titleService) {
        this.titleService = titleService;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Long id = jobExecutionContext.getJobDetail().getJobDataMap().getLong("titleId");
        Title title = this.titleService.findOne(id);

        if (title == null) {
            // title was probably deleted since the last time the job ran, abort
            return;
        }

        TuneInResponse tuneInResponse = null;
        int nextFetch = 10;
        try {
            tuneInResponse = this.getNowPlayingInfo(title.getUri());

            if (!title.getTitle().equals(tuneInResponse.getHeader().getSubtitle())) {
                title.setTitle(tuneInResponse.getHeader().getSubtitle());

                // after title has been updated, persist changes
                this.titleService.updateTitle(title);

                log.info("TuneIn fetcher - title: '{}', info: '{}'.", title.getId(), title.getTitle());
            }

            nextFetch = tuneInResponse.getTtl();
        } catch (Exception e) {
            log.error("Exception occurred fetching for title: '{}', exception: '{}'", title.getId(), e.getMessage());
        }

        // re-schedule job
        Trigger trigger = TriggerBuilder.newTrigger()
                .withIdentity(title.getId().toString())
                .forJob(title.getId().toString())
                .startAt(DateBuilder.futureDate(nextFetch, DateBuilder.IntervalUnit.SECOND))
                .build();

        try {
            jobExecutionContext.getScheduler().rescheduleJob(TriggerKey.triggerKey(title.getId().toString()), trigger);
        } catch (SchedulerException e) {
            log.error(
                    "Exception occurred rescheduling fetching job for title: '{}', exception: '{}'.",
                    title.getId(),
                    e.getMessage());
        }
    }

    private TuneInResponse getNowPlayingInfo(String stationId) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(String.format(NOW_PLAYING_URL_FORMAT, stationId, Math.random()), TuneInResponse.class);
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class TuneInResponse {
        private TuneInHeader header;

        // how many seconds to wait before performing another fetch
        private int ttl;

        public TuneInResponse() {}

        public TuneInHeader getHeader() {
            return header;
        }

        @JsonProperty("Header")
        public void setHeader(TuneInHeader header) {
            this.header = header;
        }

        public int getTtl() {
            return ttl;
        }

        @JsonProperty("Ttl")
        public void setTtl(int ttl) {
            this.ttl = ttl;
        }
    }

    private static class TuneInHeader {
        private String title;
        private String subtitle;

        public TuneInHeader() {}

        public String getTitle() {
            return title;
        }

        @JsonProperty("Title")
        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubtitle() {
            return subtitle;
        }

        @JsonProperty("Subtitle")
        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }
    }
}
