package com.example.domain.job;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created on 02/06/16.
 */

@Service
public class JobService {

    private Scheduler scheduler;

    private JobResultRepository jobResultRepository;

    @Autowired
    public JobService(Scheduler scheduler, JobResultRepository jobResultRepository) {
        this.scheduler = scheduler;
        this.jobResultRepository = jobResultRepository;
    }

    public void createAndStartJob(CreateJob createJob) throws SchedulerException {
        JobDetail job = null;
        Trigger trigger = null;

        switch (createJob.getType()) {

            case LOCAL_SCAN:
                job = JobBuilder.newJob(LocalScanJob.class)
                        .build();

                trigger = TriggerBuilder.newTrigger()
                        .startNow()
                        .build();
                break;
        }

        this.scheduler.scheduleJob(job, trigger);
    }
}
