package com.example.domain.client;

import com.example.domain.playlist.PlaylistNotification;
import com.example.domain.title.TitleNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.fn.Consumer;

import static reactor.bus.selector.Selectors.$;

/**
 * Created on 05/06/16.
 */

@Service
public class ClientNotificationService implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(ClientNotificationService.class);

    private EventBus eventBus;

    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    public ClientNotificationService(EventBus eventBus, SimpMessagingTemplate messagingTemplate) {
        this.eventBus = eventBus;
        this.messagingTemplate = messagingTemplate;
    }

    @Override
    public void run(String... strings) throws Exception {
        final ClientNotificationService self = this;

        this.eventBus.on($("title"), new Consumer<Event<TitleNotification>>() {
            @Override
            public void accept(Event<TitleNotification> event) {
                // forward title updates to websocket clients
                self.messagingTemplate.convertAndSend("/topic/title", event.getData());
            }
        });

        this.eventBus.on($("lobby"), new Consumer<Event<LobbyNotification>>() {
            @Override
            public void accept(Event<LobbyNotification> event) {
                LobbyNotification notification = event.getData();
                self.messagingTemplate.convertAndSend(String.format("/topic/lobby/%d", notification.getId()), notification);
            }
        });

        this.eventBus.on($("playlist"), new Consumer<Event<PlaylistNotification>>() {
            @Override
            public void accept(Event<PlaylistNotification> event) {
                PlaylistNotification notification = event.getData();
                self.messagingTemplate.convertAndSend("/topic/playlist", notification);
            }
        });
    }
}
