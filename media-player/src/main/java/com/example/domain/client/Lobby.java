package com.example.domain.client;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created on 06/06/16.
 */

@Entity
public class Lobby {
    @Id
    @GeneratedValue
    private Long id;

    private Long nowPlayingTitleId = null;

    private Long nowPlayingPlaylistId = null;

    private Integer nowPlayingTitleIndex = null;

    // volume as a percentage
    private Long volume = 50L;

    // currently selected playlist in the lobby
    private Long playlistId = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNowPlayingTitleId() {
        return nowPlayingTitleId;
    }

    public void setNowPlayingTitleId(Long nowPlayingTitleId) {
        this.nowPlayingTitleId = nowPlayingTitleId;
    }

    public Long getNowPlayingPlaylistId() {
        return nowPlayingPlaylistId;
    }

    public void setNowPlayingPlaylistId(Long nowPlayingPlaylistId) {
        this.nowPlayingPlaylistId = nowPlayingPlaylistId;
    }

    public Integer getNowPlayingTitleIndex() {
        return nowPlayingTitleIndex;
    }

    public void setNowPlayingTitleIndex(Integer nowPlayingTitleIndex) {
        this.nowPlayingTitleIndex = nowPlayingTitleIndex;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    public Long getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(Long playlistId) {
        this.playlistId = playlistId;
    }
}
