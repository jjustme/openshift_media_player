package com.example.domain.client;

import com.example.domain.playlist.Playlist;
import com.example.domain.playlist.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created on 22/06/16.
 */

@Service
public class NowPlayingService {

    private LobbyService lobbyService;

    private PlaylistService playlistService;

    private PlatformTransactionManager transactionManager;

    @Autowired
    public NowPlayingService(LobbyService lobbyService, PlaylistService playlistService, PlatformTransactionManager transactionManager) {
        this.lobbyService = lobbyService;
        this.playlistService = playlistService;
        this.transactionManager = transactionManager;
    }

    @Transactional
    public void setNowPlayingTitle(Lobby lobby, Long playlistId, Integer titleIndex) throws Exception {
        if (titleIndex < 0) {
            throw new Exception("Playlist index cannot be negative.");
        }

        if (playlistId == null) {
            throw new Exception("No playlist specified to play from.");
        }

        Playlist playlist = this.playlistService.findOne(playlistId);

        if (playlist == null) {
            throw new Exception(String.format("Playlist with id '%s' not found.", playlistId));
        }

        if (titleIndex >= playlist.getTitles().size()) {
            throw new Exception(String.format("Playlist index '%s' is greater than the number of titles in playlist '%s'.", titleIndex, playlist.getTitles().size()));
        }

        lobby.setNowPlayingTitleId(playlist.getTitles().get(titleIndex).getId());
        lobby.setNowPlayingPlaylistId(playlist.getId());
        lobby.setNowPlayingTitleIndex(titleIndex);

        this.lobbyService.updateLobby(lobby);
    }

    public void removePlaylistTitle(final Playlist playlist, final Integer titleIndex) throws Exception {
        TransactionTemplate transactionTemplate = new TransactionTemplate(this.transactionManager);
        List<Lobby> lobbies = transactionTemplate.execute(new TransactionCallback<List<Lobby>>() {
            @Override
            public List<Lobby> doInTransaction(TransactionStatus transactionStatus) {
                try {
                    return doRemovePlaylistTitle(playlist, titleIndex);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });

        // clients need to be informed of changes to the playlist and lobbies
        this.playlistService.notifyUpdate(playlist.getId());

        for (Lobby lobby : lobbies) {
            this.lobbyService.notifyUpdate(lobby.getId());
        }
    }

    /**
     * @return THe list of lobbies that were updated by this method. Changes to the lobbies need to pushed to the client after the transaction has commited.
     */
    public List<Lobby> doRemovePlaylistTitle(Playlist playlist, Integer titleIndex) throws Exception {
        if (titleIndex == null || titleIndex < 0) {
            throw new Exception("Playlist index must be a positive integer.");
        }

        playlist.getTitles().remove(titleIndex.intValue());
        this.playlistService.updatePlaylist(playlist, false);

        List<Lobby> lobbies = this.lobbyService.findByNowPlayingPlaylistId(playlist.getId());
        List<Lobby> updatedLobbies = new ArrayList<>();

        for (Lobby lobby : lobbies) {
            if (titleIndex < lobby.getNowPlayingTitleIndex()) {
                // removing a title before the currently playing title
                // reduce the playing title index by 1
                lobby.setNowPlayingTitleIndex(lobby.getNowPlayingTitleIndex() - 1);
            } else if (Objects.equals(titleIndex, lobby.getNowPlayingTitleIndex())) {
                // removing the currently playing title
                lobby.setNowPlayingTitleIndex(null);
                lobby.setNowPlayingPlaylistId(null);
            } else {
                // no need to update lobby if nothing has changed
                continue;
            }

            this.lobbyService.updateLobby(lobby, false);
            updatedLobbies.add(lobby);
        }

        return updatedLobbies;
    }
}
