package com.example.domain.client;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created on 06/06/16.
 */
public interface LobbyRepository extends JpaRepository<Lobby, Long> {
    List<Lobby> findByNowPlayingPlaylistId(Long playlistId);
}
