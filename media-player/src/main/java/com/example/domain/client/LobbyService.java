package com.example.domain.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.bus.Event;
import reactor.bus.EventBus;

import java.util.List;

/**
 * Created on 06/06/16.
 */

@Service
public class LobbyService {

    private LobbyRepository lobbyRepository;

    private EventBus eventBus;

    @Autowired
    public LobbyService(LobbyRepository lobbyRepository, EventBus eventBus) {
        this.lobbyRepository = lobbyRepository;
        this.eventBus = eventBus;
    }

    public List<Lobby> findAll() {
        return this.lobbyRepository.findAll();
    }

    public boolean exists(Long id) {
        return this.lobbyRepository.exists(id);
    }

    public Lobby findOne(Long id) {
        return this.lobbyRepository.findOne(id);
    }

    public List<Lobby> findByNowPlayingPlaylistId(Long playlistId) {
        return this.lobbyRepository.findByNowPlayingPlaylistId(playlistId);
    }

    public Lobby addLobby(Lobby lobby) {
        lobby.setId(null);
        lobby = this.lobbyRepository.save(lobby);
        return lobby;
    }

    public Lobby updateLobby(Lobby lobby) {
        return this.updateLobby(lobby, true);
    }

    public Lobby updateLobby(Lobby lobby, boolean notify) {
        lobby = this.lobbyRepository.save(lobby);

        if (notify) {
            this.notify(lobby.getId(), LobbyNotification.Action.UPDATE);
        }

        return lobby;
    }

    public void deleteLobby(Long id) {
        this.lobbyRepository.delete(id);
    }

    public void deleteAll() {
        this.lobbyRepository.deleteAll();
    }

    public void notifyUpdate(Long id) {
        this.notify(id, LobbyNotification.Action.UPDATE);
    }

    private void notify(Long id, LobbyNotification.Action action) {
        this.eventBus.notify("lobby", Event.wrap(new LobbyNotification(id, action)));
    }
}
