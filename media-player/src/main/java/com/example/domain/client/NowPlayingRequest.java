package com.example.domain.client;

/**
 * Created on 22/06/16.
 */
public class NowPlayingRequest {
    private Long playlistId;

    private Integer titleIndex;

    public Long getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(Long playlistId) {
        this.playlistId = playlistId;
    }

    public Integer getTitleIndex() {
        return titleIndex;
    }

    public void setTitleIndex(Integer titleIndex) {
        this.titleIndex = titleIndex;
    }
}
