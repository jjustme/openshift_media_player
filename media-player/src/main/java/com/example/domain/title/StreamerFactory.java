package com.example.domain.title;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * Created on 13/06/16.
 */

@Component
public class StreamerFactory {

    private AutowireCapableBeanFactory beanFactory;

    @Autowired
    public StreamerFactory(AutowireCapableBeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    public Streamer resolveStreamer(Provider provider) {
        Streamer streamer = null;

        switch (provider) {
            case TUNEIN:
                streamer = (Streamer) this.beanFactory.autowire(TuneInStreamer.class, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, false);
                break;
            case LOCAL:
                streamer = (Streamer) this.beanFactory.autowire(LocalFileStreamer.class, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, false);
            default:
                break;
        }

        return streamer;
    }
}
