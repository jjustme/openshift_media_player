package com.example.domain.title;

import java.io.InputStream;

/**
 * Created on 13/06/16.
 */
public interface Streamer {
    InputStream getStream(String uri) throws Exception;
}
