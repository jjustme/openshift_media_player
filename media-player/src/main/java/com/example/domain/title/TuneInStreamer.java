package com.example.domain.title;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created on 13/06/16.
 */
public class TuneInStreamer implements Streamer {
    private static final String TUNEIN_BASE_URL  = "http://tunein.com";
    private static final String STREAM_URL_FORMAT = "http://tunein.com/tuner/tune/?stationId=%s&tuneType=Station";

    private static OkHttpClient httpClient = new OkHttpClient();

    @Autowired
    public TuneInStreamer() {}

    public InputStream getStream(String stationId) throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        TuneInBroadcast response = restTemplate.getForObject(String.format(STREAM_URL_FORMAT, stationId), TuneInBroadcast.class);

        String streamUrl = this.getStreamUrl(response);

        TuneInStreamsResponse streamsResponse = restTemplate.getForObject(streamUrl, TuneInStreamsResponse.class);
        List<StreamResponse> streams = streamsResponse.getStreams();
        Collections.sort(streams, new Comparator<StreamResponse>() {
            @Override
            public int compare(StreamResponse a, StreamResponse b) {
                if (a.getBandwidth() != b.getBandwidth()) {
                    return b.getBandwidth() - a.getBandwidth();
                } else {
                    return b.getReliability() - a.getReliability();
                }
            }
        });

        if (streams.size() > 0) {
            return openStream(streams.get(0).getUrl());
        } else {
            throw new Exception("No streams found for station: " + stationId);
        }
    }

    private String getStreamUrl(TuneInBroadcast station) throws Exception {
        String streamUrl = station.getStreamUrl();
        if (streamUrl.equals("")) {
            if (station.getNavUrl().equals("")) {
                throw new Exception("Stream url and nav url both empty.");
            }

            RestTemplate restTemplate = new RestTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.add("X-Requested-With", "XMLHttpRequest");

            HttpEntity<?> entity = new HttpEntity<>(headers);
            String url = TUNEIN_BASE_URL + station.getNavUrl();
            TuneInNavResponse navResponse = restTemplate.exchange(url, HttpMethod.GET, entity, TuneInNavResponse.class).getBody();

            streamUrl = navResponse.getPayload().getStation().getBroadcast().getStreamUrl();
        }

        if (streamUrl.startsWith("//")) {
            streamUrl = "http:" + streamUrl;
        }

        return streamUrl;
    }

    private InputStream openStream(String url) throws IOException {
        Request request = new Request.Builder().url(url).build();
        Response response = httpClient.newCall(request).execute();
        return response.body().byteStream();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class TuneInBroadcast {
        private String streamUrl;
        private String navUrl;

        public TuneInBroadcast() {}

        public String getStreamUrl() {
            return streamUrl;
        }

        @JsonProperty("StreamUrl")
        public void setStreamUrl(String streamUrl) {
            this.streamUrl = streamUrl;
        }

        public String getNavUrl() {
            return navUrl;
        }

        @JsonProperty("NavUrl")
        public void setNavUrl(String navUrl) {
            this.navUrl = navUrl;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TuneInNavResponse {
        private TuneInPayload payload;

        public TuneInPayload getPayload() {
            return payload;
        }

        @JsonProperty("payload")
        public void setPayload(TuneInPayload payload) {
            this.payload = payload;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TuneInPayload {
        private TuneInStation station;

        public TuneInStation getStation() {
            return station;
        }

        @JsonProperty("Station")
        public void setStation(TuneInStation station) {
            this.station = station;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class TuneInStation {
        private TuneInBroadcast broadcast;

        public TuneInBroadcast getBroadcast() {
            return broadcast;
        }

        @JsonProperty("broadcast")
        public void setBroadcast(TuneInBroadcast broadcast) {
            this.broadcast = broadcast;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class TuneInStreamsResponse {
        private List<StreamResponse> streams;

        public List<StreamResponse> getStreams() {
            return streams;
        }

        @JsonProperty("Streams")
        public void setStreams(List<StreamResponse> streams) {
            this.streams = streams;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class StreamResponse {
        private int reliability;

        private int bandwidth;

        private String url;

        public int getReliability() {
            return reliability;
        }

        @JsonProperty("Reliability")
        public void setReliability(int reliability) {
            this.reliability = reliability;
        }

        public int getBandwidth() {
            return bandwidth;
        }

        @JsonProperty("Bandwidth")
        public void setBandwidth(int bandwidth) {
            this.bandwidth = bandwidth;
        }

        public String getUrl() {
            return url;
        }

        @JsonProperty("Url")
        public void setUrl(String url) {
            this.url = url;
        }
    }
}
