package com.example.domain.title;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.bus.Event;
import reactor.bus.EventBus;

import java.util.List;

/**
 * Created on 02/06/16.
 */

@Service
public class TitleService {

    private TitleRepository titleRepository;

    private EventBus eventBus;

    @Autowired
    public TitleService(TitleRepository titleRepository, EventBus eventBus) {
        this.titleRepository = titleRepository;
        this.eventBus = eventBus;
    }

    public boolean exists(Long id) {
        return this.titleRepository.exists(id);
    }

    public Title findOne(Long id) {
        return this.titleRepository.findOne(id);
    }

    public List<Title> searchTitles(String query) {
        return this.titleRepository.findByUriIgnoreCaseContaining(query);
    }

    public List<Title> findAll() {
        return this.titleRepository.findAll();
    }

    public List<Title> findByProvider(Provider provider) {
        return this.titleRepository.findByProvider(provider);
    }

    public Title addTitle(Title title) {
        title.setId(null); // prevent accidental updateTitle instead of add
        title = this.titleRepository.save(title);
        this.notify(title.getId(), TitleNotification.Action.CREATE);
        return title;
    }

    public Title updateTitle(Title title) {
        title = this.titleRepository.save(title);
        this.notify(title.getId(), TitleNotification.Action.UPDATE);
        return title;
    }

    public Title addIfNotExists(Title title) {
        Title existingTitle = this.titleRepository.findByUriAndProvider(title.getUri(), title.getProvider());
        if (existingTitle == null) {
            title = this.addTitle(title);
            return title;
        } else {
            return existingTitle;
        }
    }

    public void delete(Long id) {
        this.titleRepository.delete(id);
        this.notify(id, TitleNotification.Action.DELETE);
    }

    public void deleteAll() {
        this.titleRepository.deleteAll();
    }

    private void notify(Long id, TitleNotification.Action action) {
        this.eventBus.notify("title", Event.wrap(new TitleNotification(id, action)));
    }
}
