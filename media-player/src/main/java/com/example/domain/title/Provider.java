package com.example.domain.title;

/**
 * Created on 28/05/16.
 */
public enum Provider {
    LOCAL,
    TUNEIN,
    YOUTUBE
}
