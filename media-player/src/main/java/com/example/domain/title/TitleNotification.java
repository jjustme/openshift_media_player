package com.example.domain.title;

/**
 * Created on 04/06/16.
 */
public class TitleNotification {

    public enum Action {
        CREATE,
        UPDATE,
        DELETE
    }

    private Long id;
    private Action action;

    public TitleNotification(Long id, Action action) {
        this.id = id;
        this.action = action;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
