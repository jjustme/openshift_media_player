package com.example.domain.title;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 * Created on 13/06/16.
 */
public class LocalFileStreamer implements Streamer {
    @Override
    public InputStream getStream(String uri) throws Exception {
        return new FileInputStream(uri);
    }
}
