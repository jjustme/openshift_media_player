package com.example.domain.title;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created on 28/05/16.
 */
public interface TitleRepository extends JpaRepository<Title, Long> {
    Title findByUriAndProvider(String uri, Provider provider);
    List<Title> findByProvider(Provider provider);
    List<Title> findByUriIgnoreCaseContaining(String uri);
}
