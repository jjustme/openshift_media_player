package com.example.domain.title;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created on 28/05/16.
 */

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"provider", "uri"})})
public class Title {
    @Id
    @GeneratedValue
    private Long id;

    private String title = "";

    @NotNull
    private Boolean isStream = Boolean.TRUE;

    @NotNull
    private String uri;

    @NotNull
    private Provider provider;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getIsStream() {
        return isStream;
    }

    public void setIsStream(Boolean stream) {
        isStream = stream;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }
}
