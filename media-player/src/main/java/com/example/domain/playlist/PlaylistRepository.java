package com.example.domain.playlist;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created on 06/06/16.
 */
public interface PlaylistRepository extends JpaRepository<Playlist, Long> {
}
