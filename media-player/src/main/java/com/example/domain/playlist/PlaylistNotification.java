package com.example.domain.playlist;

/**
 * Created on 06/06/16.
 */
public class PlaylistNotification {
    public enum Action {
        UPDATE,
        DELETE
    }

    private Long id;

    private Action action;

    public PlaylistNotification(Long id, Action action) {
        this.id = id;
        this.action = action;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
