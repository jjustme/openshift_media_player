package com.example.domain.playlist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.bus.Event;
import reactor.bus.EventBus;

import java.util.List;

/**
 * Created on 06/06/16.
 */

@Service
public class PlaylistService {

    private PlaylistRepository playlistRepository;

    private EventBus eventBus;

    @Autowired
    public PlaylistService(PlaylistRepository playlistRepository, EventBus eventBus) {
        this.playlistRepository = playlistRepository;
        this.eventBus = eventBus;
    }

    public boolean exists(Long id) {
        return this.playlistRepository.exists(id);
    }

    public List<Playlist> findAll() {
        return this.playlistRepository.findAll();
    }

    public Playlist findOne(Long id) {
        return this.playlistRepository.findOne(id);
    }

    public Playlist addPlaylist(Playlist playlist) {
        playlist.setId(null);
        return this.playlistRepository.save(playlist);
    }

    public Playlist updatePlaylist(Playlist playlist) {
        return this.updatePlaylist(playlist, true);
    }

    public Playlist updatePlaylist(Playlist playlist, boolean notify) {
        playlist = this.playlistRepository.save(playlist);

        if (notify) {
            this.notifyUpdate(playlist.getId());
        }

        return playlist;
    }

    public void delete(Long id) {
        this.playlistRepository.delete(id);
        this.notify(id, PlaylistNotification.Action.DELETE);
    }


    public void deleteAll() {
        this.playlistRepository.deleteAll();
    }

    public void notifyUpdate(Long id) {
        this.notify(id, PlaylistNotification.Action.UPDATE);
    }

    private void notify(Long id, PlaylistNotification.Action action) {
        this.eventBus.notify("playlist", Event.wrap(new PlaylistNotification(id, action)));
    }
}
