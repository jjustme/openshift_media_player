package com.example.domain.playlist;

import com.example.domain.title.Title;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 06/06/16.
 */

@Entity
public class Playlist {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToMany
    private List<Title> titles = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Title> getTitles() {
        return titles;
    }

    public void setTitles(List<Title> titles) {
        this.titles = titles;
    }
}
