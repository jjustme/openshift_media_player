package com.example.controllers;

import com.example.domain.title.Streamer;
import com.example.domain.title.StreamerFactory;
import com.example.domain.title.Title;
import com.example.domain.title.TitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created on 13/06/16.
 */

@RestController
@RequestMapping("/api/play")
public class PlayController {

    private TitleService titleService;

    private StreamerFactory streamerFactory;

    @Autowired
    public PlayController(TitleService titleService, StreamerFactory streamerFactory) {
        this.titleService = titleService;
        this.streamerFactory = streamerFactory;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> streamTitle(@PathVariable Long id) {
        Title title = this.titleService.findOne(id);

        if (title == null) {
            return ResponseEntity.notFound().build();
        }

        Streamer streamer = this.streamerFactory.resolveStreamer(title.getProvider());

        try {
            InputStreamResource streamResource = new InputStreamResource(streamer.getStream(title.getUri()));
            return new ResponseEntity<Object>(streamResource, HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
