package com.example.controllers;

import com.example.domain.client.Lobby;
import com.example.domain.client.NowPlayingRequest;
import com.example.domain.client.LobbyService;
import com.example.domain.client.NowPlayingService;
import com.example.domain.playlist.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created on 06/06/16.
 */

@RestController
@RequestMapping("/api/lobbies")
public class LobbyController {

    private LobbyService lobbyService;

    private PlaylistService playlistService;

    private NowPlayingService nowPlayingService;

    @Autowired
    public LobbyController(LobbyService lobbyService, PlaylistService playlistService, NowPlayingService nowPlayingService) {
        this.lobbyService = lobbyService;
        this.playlistService = playlistService;
        this.nowPlayingService = nowPlayingService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getAllLobbies() {
        return ResponseEntity.ok(this.lobbyService.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getLobby(@PathVariable Long id) {
        Lobby lobby = this.lobbyService.findOne(id);

        if (lobby == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(lobby);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> createLobby() {
        Lobby lobby = new Lobby();
        lobby = this.lobbyService.addLobby(lobby);
        return ResponseEntity.status(HttpStatus.CREATED).body(lobby.getId());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteLobby(@PathVariable Long id) {
        if (!this.lobbyService.exists(id)) {
            return ResponseEntity.notFound().build();
        }

        this.lobbyService.deleteLobby(id);

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}/now-playing", method = RequestMethod.PUT)
    public ResponseEntity<?> setNowPlaying(@PathVariable Long id, @RequestBody(required = false) NowPlayingRequest nowPlayingRequest) {
        Lobby lobby = this.lobbyService.findOne(id);

        if (lobby == null) {
            return ResponseEntity.notFound().build();
        }

        if (nowPlayingRequest == null || nowPlayingRequest.getPlaylistId() == null || nowPlayingRequest.getTitleIndex() == null) {
            lobby.setNowPlayingTitleId(null);
            lobby.setNowPlayingPlaylistId(null);
            lobby.setNowPlayingTitleIndex(null);
            this.lobbyService.updateLobby(lobby);
            return ResponseEntity.noContent().build();
        }

        try {
            this.nowPlayingService.setNowPlayingTitle(lobby, nowPlayingRequest.getPlaylistId(), nowPlayingRequest.getTitleIndex());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}/volume", method = RequestMethod.PUT)
    public ResponseEntity<?> setVolume(@PathVariable Long id, @RequestBody Long volume) {
        if (volume < 0 || volume > 100) {
            return ResponseEntity.badRequest().build();
        }

        Lobby lobby = this.lobbyService.findOne(id);

        if (lobby == null) {
            return ResponseEntity.notFound().build();
        }

        lobby.setVolume(volume);
        this.lobbyService.updateLobby(lobby);

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}/playlist", method = RequestMethod.PUT)
    public ResponseEntity<?> setPlaylist(@PathVariable Long id, @RequestBody Long playlistId) {
        Lobby lobby = this.lobbyService.findOne(id);

        if (lobby == null) {
            return ResponseEntity.notFound().build();
        }
;
        if (!this.playlistService.exists(playlistId)) {
            return ResponseEntity.notFound().build();
        }

        lobby.setPlaylistId(playlistId);;
        this.lobbyService.updateLobby(lobby);

        return ResponseEntity.noContent().build();
    }
}
