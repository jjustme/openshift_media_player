package com.example.controllers;

import com.example.domain.client.NowPlayingService;
import com.example.domain.playlist.Playlist;
import com.example.domain.playlist.PlaylistService;
import com.example.domain.title.Title;
import com.example.domain.title.TitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created on 06/06/16.
 */

@RestController
@RequestMapping("/api/playlists")
public class PlaylistController {

    private PlaylistService playlistService;

    private TitleService titleService;

    private NowPlayingService nowPlayingService;

    @Autowired
    public PlaylistController(PlaylistService playlistService, TitleService titleService, NowPlayingService nowPlayingService) {
        this.playlistService = playlistService;
        this.titleService = titleService;
        this.nowPlayingService = nowPlayingService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getAllPlaylists() {
        return ResponseEntity.ok(this.playlistService.findAll());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getPlaylist(@PathVariable Long id) {
        Playlist playlist = this.playlistService.findOne(id);

        if (playlist == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(playlist);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> createPlaylist() {
        Playlist playlist = new Playlist();
        playlist = this.playlistService.addPlaylist(playlist);
        return ResponseEntity.status(HttpStatus.CREATED).body(playlist.getId());
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deletePlaylist(@PathVariable Long id) {
        if (!this.playlistService.exists(id)) {
            return ResponseEntity.notFound().build();
        }

        try {
            this.playlistService.delete(id);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}/add-title", method = RequestMethod.PUT)
    public ResponseEntity<?> addTitle(@PathVariable Long id, @RequestBody Long titleId) {
        Playlist playlist = this.playlistService.findOne(id);

        if (playlist == null) {
            return ResponseEntity.badRequest().build();
        }

        Title title = this.titleService.findOne(titleId);

        if (title == null) {
            return ResponseEntity.badRequest().build();
        }

        playlist.getTitles().add(title);
        this.playlistService.updatePlaylist(playlist);

        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/{id}/remove-title", method = RequestMethod.PUT)
    public ResponseEntity<?> removeTitle(@PathVariable Long id, @RequestBody Integer titleIndex) {
        if (titleIndex < 0) {
            return ResponseEntity.badRequest().build();
        }

        Playlist playlist = this.playlistService.findOne(id);

        if (playlist == null || titleIndex >= playlist.getTitles().size()) {
            return ResponseEntity.badRequest().build();
        }

        try {
            this.nowPlayingService.removePlaylistTitle(playlist, titleIndex);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        return ResponseEntity.noContent().build();
    }
}
