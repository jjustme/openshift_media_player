package com.example.controllers;

import com.example.domain.title.Provider;
import com.example.domain.title.Title;
import com.example.domain.title.TitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Created on 28/05/16.
 */

@RestController
@RequestMapping("/api/titles")
public class TitleController {

    private TitleService titleService;

    @Autowired
    public TitleController(TitleService titleService) {
        this.titleService = titleService;
    }

    @RequestMapping(value = "", method = GET)
    public ResponseEntity<?> queryTitles(@RequestParam(value = "q", required = false) String query) {
        // TODO: paging
        if (query != null) {
            return ResponseEntity.ok(this.titleService.searchTitles(query));
        }

        return ResponseEntity.ok(this.titleService.findAll());
    }

    @RequestMapping(value = "/{id}", method = GET)
    public ResponseEntity<?> getTitle(@PathVariable Long id) {
        Title title = this.titleService.findOne(id);
        if (title == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(title);
    }

    @RequestMapping(method = POST)
    public ResponseEntity<?> addTitle(@RequestBody Title title) {
        if (title.getProvider() == Provider.LOCAL) {
            return ResponseEntity.badRequest().body("Local titles cannot be added through the API.");
        }

        try {
            title = this.titleService.addTitle(title);
        } catch (EmptyResultDataAccessException exception) {
            return ResponseEntity.badRequest().build();
        } catch (DataIntegrityViolationException exception) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(title);
    }

    @RequestMapping(value = "/{id}", method = DELETE)
    public ResponseEntity<?> deleteTitle(@PathVariable Long id) {
        if (!this.titleService.exists(id)) {
            return ResponseEntity.notFound().build();
        }

        try {
            this.titleService.delete(id);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.noContent().build();
    }
}
