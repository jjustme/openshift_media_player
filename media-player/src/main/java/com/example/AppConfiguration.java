package com.example;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created on 01/06/16.
 * Designed to pick up config values in application.properties file from the folder the executing jar is in
 */

@Component
@ConfigurationProperties("user")
public class AppConfiguration {

    private String paths;

    public String getPaths() {
        return this.paths;
    }

    public void setPaths(String paths) {
        this.paths = paths;
    }
}
