package com.example;

import com.example.domain.client.Lobby;
import com.example.domain.client.LobbyService;
import com.example.domain.client.NowPlayingService;
import com.example.domain.playlist.Playlist;
import com.example.domain.playlist.PlaylistService;
import com.example.domain.title.Provider;
import com.example.domain.title.Title;
import com.example.domain.title.TitleService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 25/06/16.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MediaPlayerApplication.class)
public class NowPlayingServiceTests {

    @Autowired
    private TitleService titleService;

    @Autowired
    private PlaylistService playlistService;

    @Autowired
    private LobbyService lobbyService;

    @Autowired
    private NowPlayingService nowPlayingService;

    @Before
    public void setup() {
        this.lobbyService.deleteAll();;
        this.playlistService.deleteAll();
        this.titleService.deleteAll();
    }

    @Test
    public void changeNowPlayingTitleCorrect() throws Exception {
        Lobby lobby = new Lobby();

        lobby = this.lobbyService.addLobby(lobby);

        Playlist playlist = this.getPlaylistWithTitles(5);
        this.nowPlayingService.setNowPlayingTitle(lobby, playlist.getId(), 0);

        lobby = this.lobbyService.findOne(lobby.getId());
        Assert.assertEquals(playlist.getTitles().get(0).getId(), lobby.getNowPlayingTitleId());
        Assert.assertEquals(playlist.getId(), lobby.getNowPlayingPlaylistId());
        Assert.assertTrue(Long.compare(0, lobby.getNowPlayingTitleIndex()) == 0);

        // change playlist
        playlist = this.getPlaylistWithTitles(2);
        int lastIndex = playlist.getTitles().size() - 1;
        this.nowPlayingService.setNowPlayingTitle(lobby, playlist.getId(), lastIndex);

        lobby = this.lobbyService.findOne(lobby.getId());
        Assert.assertEquals(playlist.getTitles().get(lastIndex).getId(), lobby.getNowPlayingTitleId());
        Assert.assertEquals(playlist.getId(), lobby.getNowPlayingPlaylistId());
        Assert.assertTrue(Long.compare(lastIndex, lobby.getNowPlayingTitleIndex()) == 0);
    }

    @Test
    public void removeTitleFromPlaylistCorrect() throws Exception {
        Playlist playlist = this.getPlaylistWithTitles(10);

        List<Long> lobbyIds = new ArrayList<>(playlist.getTitles().size());
        for (int i = 0; i < playlist.getTitles().size(); i++) {
            Lobby lobby = new Lobby();
            Title title = playlist.getTitles().get(i);
            lobby.setNowPlayingTitleId(title.getId());
            lobby.setNowPlayingPlaylistId(playlist.getId());
            lobby.setNowPlayingTitleIndex(i);

            lobbyIds.add(this.lobbyService.addLobby(lobby).getId());
        }

        int removeTitleIndex = 5;
        // keep a copy of the titles as the playlist will be modified
        List<Title> titles = new ArrayList<>(playlist.getTitles());
        this.nowPlayingService.removePlaylistTitle(playlist, removeTitleIndex);

        // get updated lobbies
        List<Lobby> updatedLobbies = new ArrayList<>(lobbyIds.size());
        for (Long lobbyId: lobbyIds) {
            Lobby updatedLobby = this.lobbyService.findOne(lobbyId);
            updatedLobbies.add(updatedLobby);
        }

        for (int i = 0; i < updatedLobbies.size(); i++) {
            Lobby lobby = updatedLobbies.get(i);

            Title title = titles.get(i);
            Assert.assertEquals(title.getId(), lobby.getNowPlayingTitleId());

            if (i == removeTitleIndex) {
                Assert.assertEquals(null, lobby.getNowPlayingTitleIndex());
                Assert.assertEquals(null, lobby.getNowPlayingPlaylistId());
            } else if (i > removeTitleIndex) {
                // ensure now playing index is reduced by one when a title above it in the playlist is removed
                Assert.assertTrue(Long.compare(i - 1, lobby.getNowPlayingTitleIndex()) == 0);
                Assert.assertEquals(playlist.getId(), lobby.getNowPlayingPlaylistId());
            } else {
                Assert.assertTrue(Long.compare(i, lobby.getNowPlayingTitleIndex()) == 0);
                Assert.assertEquals(playlist.getId(), lobby.getNowPlayingPlaylistId());
            }
        }
    }

    public Playlist getPlaylistWithTitles(int amount) {
        Playlist playlist = new Playlist();

        for (int i = 0; i < amount; i++) {
            Title title = new Title();
            title.setUri(String.format("http://%s.com", Math.random()));
            title.setIsStream(true);
            title.setProvider(Provider.TUNEIN);

            this.titleService.addTitle(title);
            playlist.getTitles().add(title);
        }

        return this.playlistService.addPlaylist(playlist);
    }
}
