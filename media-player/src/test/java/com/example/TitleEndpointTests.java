package com.example;

import com.example.domain.title.Provider;
import com.example.domain.title.Title;
import com.example.domain.title.TitleService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MediaPlayerApplication.class)
@WebAppConfiguration
public class TitleEndpointTests {

	@Autowired
	private WebApplicationContext applicationContext;

	@Autowired
	private TitleService titleService;

	private MockMvc mockMvc;

	private ObjectMapper objectMapper = new ObjectMapper();

	@Before
	public void setup() {
		this.mockMvc = webAppContextSetup(this.applicationContext).build();
		this.titleService.deleteAll();
	}

	@Test
	public void getNonExistingTitleNotFound() throws Exception {
		this.mockMvc
				.perform(get("/api/titles/1")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void searchTitlesCorrect() throws Exception {
		int amountOfExpectedTitles = 5;
		int amountOfUnexpectedTitles = 5;
		List<Title> expectedTitles = new ArrayList<>(amountOfExpectedTitles);
		List<Title> unexpectedTitles = new ArrayList<>(amountOfUnexpectedTitles);
		String query = "hello world";

		for (int i = 0; i < amountOfExpectedTitles; i++) {
			Title title = new Title();
			title.setIsStream(true);
			title.setUri(String.format("%s%s", query, Math.random()));
			title.setProvider(Provider.YOUTUBE);

			expectedTitles.add(this.titleService.addTitle(title));
		}

		for (int i = 0; i < amountOfUnexpectedTitles; i++) {
			Title title = new Title();
			title.setIsStream(true);
			title.setUri(String.format("%s%s", query.substring(query.length() - 2), Math.random()));
			title.setProvider(Provider.YOUTUBE);

			unexpectedTitles.add(this.titleService.addTitle(title));
		}

        String pathAndQuery = "/api/titles?q=" + query;
        String response = this.mockMvc
                .perform(get(pathAndQuery)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        List<Title> responseTitles = this.objectMapper.readValue(response, new TypeReference<List<Title>>() {});

        for (Title expectedTitle : expectedTitles) {
            boolean found = false;
            for (Title responseTitle : responseTitles) {
                if (Objects.equals(responseTitle.getId(), expectedTitle.getId())) {
                    found = true;
                    break;
                }
            }

            Assert.assertTrue(found);
        }

        for (Title unexpectedTitle : unexpectedTitles) {
            boolean found = false;
            for (Title responseTitle : responseTitles) {
                if (Objects.equals(responseTitle.getId(), unexpectedTitle.getId())) {
                    found = true;
                    break;
                }
            }

            Assert.assertFalse(found);
        }
    }

	@Test
	public void createValidTitleAndGetAndDeleteSuccess() throws Exception {
		Title title = new Title();
		title.setIsStream(true);
		title.setUri("http://testurl.com");
		title.setProvider(Provider.YOUTUBE);

		String response = this.mockMvc
				.perform(post("/api/titles")
						.contentType(MediaType.APPLICATION_JSON)
						.content(this.objectMapper.writeValueAsString(title)))
				.andExpect(status().isCreated())
				.andReturn()
				.getResponse()
				.getContentAsString();

		title = objectMapper.readValue(response, Title.class);

		this.mockMvc
				.perform(get("/api/titles/" + title.getId())
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

		this.mockMvc
				.perform(delete("/api/titles/" + title.getId()))
				.andExpect(status().isNoContent());
	}

	@Test
	public void createLocalProviderTitleBadRequest() throws Exception {
		Title title = new Title();
		title.setIsStream(true);
		title.setUri("http://testurl.com");
		title.setProvider(Provider.LOCAL);

		this.mockMvc
				.perform(post("/api/titles")
						.contentType(MediaType.APPLICATION_JSON)
						.content(this.objectMapper.writeValueAsString(title)))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void deleteNonExistingTitleNotFound() throws Exception {
		this.mockMvc
				.perform(delete("/api/titles/1"))
				.andExpect(status().isNotFound());
	}
}
