package com.example;

import com.example.domain.playlist.Playlist;
import com.example.domain.playlist.PlaylistService;
import com.example.domain.title.Provider;
import com.example.domain.title.Title;
import com.example.domain.title.TitleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created on 25/06/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MediaPlayerApplication.class)
@WebAppConfiguration
public class PlaylistEndpointTests {

    @Autowired
    private WebApplicationContext applicationContext;

    @Autowired
    private PlaylistService playlistService;

    @Autowired
    private TitleService titleService;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.applicationContext).build();
        this.playlistService.deleteAll();
    }

    @Test
    public void getNonExistingPlaylistNotFound() throws Exception {
        this.mockMvc
                .perform(get("/api/playlists/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createValidPlaylistAndGetAndDeleteSuccess() throws Exception {
        String response = this.createPlaylist();

        Long playlistId = Long.valueOf(response);

        response = this.mockMvc
                .perform(get("/api/playlists/" + playlistId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Playlist playlist = this.objectMapper.readValue(response, Playlist.class);

        Assert.assertEquals(playlistId, playlist.getId());

        this.mockMvc
                .perform(delete("/api/playlists/" + playlist.getId()))
                .andExpect(status().isNoContent());
    }

    @Test
    public void addTitleAndRemoveTitleSuccess() throws Exception {
        Long playlistId = Long.valueOf(this.createPlaylist());

        // create titles to add to the playlist
        Set<Title> titles = this.createTitles(5);

        for (Title title : titles) {
            String endpoint = String.format("/api/playlists/%d/add-title", playlistId);
            this.mockMvc
                    .perform(put(endpoint)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(this.objectMapper.writeValueAsString(title.getId())))
                    .andExpect(status().isNoContent());

            Playlist playlist = this.getPlaylist(playlistId);
            assertTrue(this.playlistContainsTitle(playlist, title));
        }

        Playlist fullPlaylist = this.getPlaylist(playlistId);
        for (Title title : titles) {
            assertTrue(this.playlistContainsTitle(fullPlaylist, title));
        }

        for (Title title : fullPlaylist.getTitles()) {
            String endpoint = String.format("/api/playlists/%d/remove-title", playlistId);
            this.mockMvc
                    .perform(put(endpoint)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(this.objectMapper.writeValueAsString(0)))
                    .andExpect(status().isNoContent());

            Playlist playlist = this.getPlaylist(playlistId);
            assertFalse(this.playlistContainsTitle(playlist, title));
        }
    }

    private boolean playlistContainsTitle(Playlist playlist, Title title) {
        boolean found = false;
        for (Title playlistTitle : playlist.getTitles()) {
            if (Objects.equals(playlistTitle.getId(), title.getId())) {
                found = true;
                break;
            }
        }

        return found;
    }

    public Playlist getPlaylist(Long playlistId) throws Exception {
        String response = this.mockMvc
                .perform(get("/api/playlists/" + playlistId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        return this.objectMapper.readValue(response, Playlist.class);
    }

    public String createPlaylist() throws Exception {
        return this.mockMvc
                .perform(post("/api/playlists")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    public Set<Title> createTitles(int amount) throws Exception {
        Set<Title> createdTitles = new HashSet<>(amount);
        for (int i = 0; i < amount; i++) {
            Title title = new Title();
            title.setIsStream(true);
            title.setUri(String.format("http://testurl.com/%s", Math.random()));
            title.setProvider(Provider.TUNEIN);

            createdTitles.add(this.titleService.addTitle(title));
        }

        return createdTitles;
    }
}
